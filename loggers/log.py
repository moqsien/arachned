import logging
from logging import handlers, getLogger


class FileFilter(logging.Filter):
    def filter(self, record):
        try:
            filter_key = record.levelname
        except AttributeError:
            filter_key = None
        if filter_key in ['WARNING', 'ERROR', 'CRITICAL']:
            result = 1
        else:
            result = 0
        return result


class Logger(object):
    # 日志级别关系映射
    level_relations = {
        'notset': logging.NOTSET,
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL
    }

    def __init__(self, filename=None, level='info', when='D', back_count=2, fmt=''):
        self.logger = getLogger()
        self.logger.setLevel(logging.INFO)
        if not fmt:
            fmt = '%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'

        format_str = logging.Formatter(fmt)  # 设置日志格式

        # 实例化TimedRotatingFileHandler
        # interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
        # S 秒、 M 分、 H 小时、 D 天、W 每星期（interval==0时代表星期一）、 midnight 每天凌晨
        if filename:
            f = handlers.TimedRotatingFileHandler(
                filename=filename, when=when, interval=1, backupCount=back_count, encoding='utf-8')  # 往文件里写入, 指定间隔时间自动生成文件的处理器
            f.setFormatter(format_str)  # 设置文件里写入的格式
            f.setLevel(self.level_relations.get(level))
            filter_ = FileFilter()
            f.addFilter(filter_)
            self.logger.addHandler(f)

        t = logging.StreamHandler()  # 往屏幕上输出
        t.setLevel(logging.INFO)
        t.setFormatter(format_str)  # 设置屏幕上显示的格式
        self.logger.addHandler(t)  # 把对象加到logger里
        logging.getLogger('apscheduler').setLevel(logging.WARNING)  # apscheduler框架中的日志不输出到屏幕

