# 用splash进行js渲染，主要用于拼多多详情图的抓取
import json
from urllib.parse import quote
from user_agent import generate_user_agent
from .req_async import requests
from .req import OS_TYPE


# splash运行的lua脚本
LUA_SCRIPT = """
function main(splash)
    splash:set_user_agent('%s')
    splash:go("%s")
    splash:wait(0.5)
    return {
    html = splash:html()
    }
end
"""

# splash请求url
SPLASH_URL = "http://*.*.*.*:*/execute?lua_source="


async def requests_splash(url, is_mobile=False):
    url_to_splash = SPLASH_URL + quote(LUA_SCRIPT % (generate_user_agent(os=OS_TYPE[is_mobile]), url))
    resp = await requests("GET", url_to_splash)
    json_data = json.loads(resp.content.decode("utf-8"))
    html = json_data.get("html")
    return html


if __name__ == '__main__':
    pass
