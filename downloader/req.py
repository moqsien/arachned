import urllib3
import requests
from lxml import etree
from .req_async import OS_TYPE
from user_agent import generate_user_agent


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def request(method, url, is_mobile=False, **kwargs):
    kwargs.setdefault("timeout", 10)
    if "headers" in kwargs:
        kwargs["headers"].setdefault("user-agent", generate_user_agent(os=OS_TYPE[is_mobile]))
    else:
        kwargs["headers"] = {
            "user-agent": generate_user_agent(os=OS_TYPE[is_mobile])
        }
    session = requests.Session()
    resp = session.request(method, url, verify=False, **kwargs)
    resp.xpath = etree.HTML(resp.text).xpath
    return resp
