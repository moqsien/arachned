from .req_async import requests as async_request
from .req import request
from .req_selenium import SeleniumRequest as Selenium
from .req_splash import requests_splash as splash
