import aiomysql


class MySQLAsync(object):
    def __init__(self, **kwargs):
        self.host = kwargs["host"]
        self.port = kwargs["port"]
        self.user = kwargs["user"]
        self.password = kwargs["password"]
        self.database = kwargs["db"]
        self.loop = kwargs["loop"]
        self.pool = None
    
    async def __aenter__(self, sql_):
        self.pool = await aiomysql.create_pool(host=self.host, 
                                          port=self.port,
                                          user=self.user,
                                          password=self.password,
                                          db=self.database,
                                          loop=self.loop)
        return self.pool
    
    async def __aexit__(self, exc_type, exc, tb):
        self.pool.close()
        await self.pool.wait_closed()
        return True
    