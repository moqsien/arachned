import abc
import asyncio
from downloader import async_request


class SpiderBase(object):
    def __init__(self, start_url):
        self.start_url = start_url
    
    async def get_response(self, url):
        resp = await async_request("GET", url)
        return resp
    
    def get_next_url(self, response):
        pass
    
    async def parse_item(self, data):
        pass
    
    async def parse_page(self, response):
        pass
    
    def crawl(self, url=None, spider_name="test", operation="test"):
        pass
    
